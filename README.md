Repository for DSG-related documents that need to be accessed publicly. 

For example, DSG-related gradle dependencies. 

To install gradle:

Ubuntu: 
sudo add-apt-repository ppa:cwchien/gradle
sudo apt-get update
sudo apt-get install gradle

MacOS: 
brew install gradle

If you need the local.properties file, please ask Dr. Kennington.
